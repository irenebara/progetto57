Progetto57
========================================================
author: Baratella Irene

Quesito 1
========================================================

```r
data <- read.table("Progetto57.csv", header=TRUE, sep=",")
```
Quesito 2
========================================================

```r
str(data)
```

```
'data.frame':	4 obs. of  15 variables:
 $ SUMLEV           : int  40 162 50 157
 $ STATE            : int  11 11 11 11
 $ COUNTY           : int  0 0 1 1
 $ PLACE            : int  0 50000 0 50000
 $ COUSUB           : int  0 0 0 0
 $ CONCIT           : int  0 0 0 0
 $ FUNCSTAT         : Factor w/ 3 levels "A","F","N": 1 3 2 3
 $ NAME             : Factor w/ 2 levels "District of Columbia",..: 1 2 1 2
 $ STNAME           : Factor w/ 1 level "District of Columbia": 1 1 1 1
 $ CENSUS2010POP    : int  601723 601723 601723 601723
 $ ESTIMATESBASE2010: int  601767 601767 601767 601767
 $ POPESTIMATE2010  : int  605125 605125 605125 605125
 $ POPESTIMATE2011  : int  619624 619624 619624 619624
 $ POPESTIMATE2012  : int  633427 633427 633427 633427
 $ POPESTIMATE2013  : int  646449 646449 646449 646449
```
Quesito 3
========================================================

```r
names(data)
```

```
 [1] "SUMLEV"            "STATE"             "COUNTY"           
 [4] "PLACE"             "COUSUB"            "CONCIT"           
 [7] "FUNCSTAT"          "NAME"              "STNAME"           
[10] "CENSUS2010POP"     "ESTIMATESBASE2010" "POPESTIMATE2010"  
[13] "POPESTIMATE2011"   "POPESTIMATE2012"   "POPESTIMATE2013"  
```
Quesito 4
========================================================

```r
numeroOccorrenze = 0
for(i in 1:4){
for(j in 1:15){
if(data[i,j] == "X"){
numeroOccorrenze = numeroOccorrenze+1
print(i)
print(j)
}}}
print(numeroOccorrenze)
```

```
[1] 0
```
Quesito 5 - Soluzione 1
========================================================

```r
s = '----------'
  numeroOccorrenze = 0
for(i in 1:3){
for(j in 2:4){
if(i<j){
if(data [i,3] == data [j,3]){
numeroOccorrenze = numeroOccorrenze+1
print(s)
print(i)
print(j)
}}}}
```

```
[1] "----------"
[1] 1
[1] 2
[1] "----------"
[1] 3
[1] 4
```

```r
print(numeroOccorrenze)
```

```
[1] 2
```
Quesito 5 - Soluzione 2
========================================================

```r
plyr::count(data$COUNTY)
```

```
  x freq
1 0    2
2 1    2
```
Quesito 5 - Soluzione 3
========================================================

```r
summary(factor(data$COUNTY))
```

```
0 1 
2 2 
```
Quesito 6 - Soluzione 1
========================================================

```r
anno2010 <- max(data[12])
anno2010
```

```
[1] 605125
```

```r
anno2011 <- max(data[13])
anno2011
```

```
[1] 619624
```

```r
anno2012 <- max(data[14])
anno2012
```

```
[1] 633427
```
Quesito 6 - Soluzione 1 - Parte 2
========================================================

```r
anno2013 <- max(data[15])
anno2013
```

```
[1] 646449
```

```r
risultato <- max(anno2010, anno2011, anno2012, anno2013)
risultato
```

```
[1] 646449
```
Quesito 6 - Soluzione 2
========================================================

```r
risultato <- max(data[12], data[13], data[14], data[15])
risultato
```

```
[1] 646449
```
Quesito 7
========================================================

```r
popolazione <- c(anno2010, anno2011, anno2012, anno2013)
popolazione
```

```
[1] 605125 619624 633427 646449
```

```r
anno <- c(2010, 2011, 2012, 2013)
anno
```

```
[1] 2010 2011 2012 2013
```
Grafico lineare
========================================================

```r
plot(anno,popolazione, xaxt='n', 
type='b', xlab="ANNO ",
ylab="POPOLAZIONE",
main="Grafico lineare dell'andamento della popolazione dal 2010 al 2013")
axis(1, at=anno)
```

![plot of chunk unnamed-chunk-12](Progetto57-figure/unnamed-chunk-12-1.png) 
Quesito 8
========================================================

```r
y <- c(anno2010, anno2011, anno2012, anno2013)
POPEST <- rep(c('POPEST2010', 'POPEST2011', 'POPEST2012', 'POPEST2013'),each=1)
m <- tapply(y, POPEST, max)
```
Grafico a barre
========================================================

```r
bp <- barplot(m, ylim=c(600000, 650000), col = rainbow(4), main="Grafico a barre dell'andamento della popolazione dal 2010 al 2013")
```

![plot of chunk unnamed-chunk-14](Progetto57-figure/unnamed-chunk-14-1.png) 
Quesito 9
========================================================

```r
POPESTIMATE <- c(anno2010, anno2011, anno2012, anno2013)
valoriPOPESTIMATE <- table(POPESTIMATE)
pct<-round((valoriPOPESTIMATE/margin.table(valoriPOPESTIMATE)*100),1)
lbls<-c("POPEST2010", "POPEST2011", "POPEST2012", "POPEST2013")
lbls<-paste(lbls, pct)
lbls<-paste(lbls,"%",sep="")
```
Grafico a torta
========================================================

```r
pie(valoriPOPESTIMATE, labels = lbls, col= rainbow(4), main="Grafico a torta dei valori POPESTIMATE DAL 2010 AL 2013")
```

![plot of chunk unnamed-chunk-16](Progetto57-figure/unnamed-chunk-16-1.png) 
