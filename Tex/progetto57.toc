\select@language {italian}
\contentsline {chapter}{Elenco delle figure}{ii}{chapter*.1}
\contentsline {chapter}{Introduzione}{4}{Item.19}
\contentsline {chapter}{\numberline {1}Importare dati in R}{5}{chapter.1}
\contentsline {section}{\numberline {1.1}Soluzione richiesta 1}{5}{section.1.1}
\contentsline {chapter}{\numberline {2}Analizzare l'oggetto data}{6}{chapter.2}
\contentsline {section}{\numberline {2.1}Soluzione richiesta 2}{6}{section.2.1}
\contentsline {chapter}{\numberline {3}Determinare il nome delle variabili presenti}{7}{chapter.3}
\contentsline {section}{\numberline {3.1}Soluzione richiesta 3}{7}{section.3.1}
\contentsline {chapter}{\numberline {4}Ricercare il valore 'X'}{8}{chapter.4}
\contentsline {section}{\numberline {4.1}Soluzione 1}{8}{section.4.1}
\contentsline {section}{\numberline {4.2}Soluzione 2}{8}{section.4.2}
\contentsline {chapter}{\numberline {5}Determinare numero di righe con lo stesso valore}{10}{chapter.5}
\contentsline {section}{\numberline {5.1}Soluzione 1}{10}{section.5.1}
\contentsline {section}{\numberline {5.2}Soluzione 2}{10}{section.5.2}
\contentsline {section}{\numberline {5.3}Soluzione 3}{11}{section.5.3}
\contentsline {chapter}{\numberline {6}Determinare valore massimo}{12}{chapter.6}
\contentsline {section}{\numberline {6.1}Soluzione 1}{12}{section.6.1}
\contentsline {section}{\numberline {6.2}Soluzione 2}{12}{section.6.2}
\contentsline {chapter}{\numberline {7}Grafico lineare}{13}{chapter.7}
\contentsline {section}{\numberline {7.1}Soluzione richiesta 7}{13}{section.7.1}
\contentsline {chapter}{\numberline {8}Grafico a barre}{15}{chapter.8}
\contentsline {section}{\numberline {8.1}Soluzione richiesta 8}{15}{section.8.1}
\contentsline {chapter}{\numberline {9}Pie chart}{16}{chapter.9}
\contentsline {section}{\numberline {9.1}Soluzione richiesta 9}{16}{section.9.1}
\contentsline {chapter}{\numberline {10}Presentazione del progetto con Shiny}{17}{chapter.10}
\contentsline {chapter}{\numberline {A}Equivalente del progetto in Excel}{19}{appendix.A}
\contentsline {section}{\numberline {A.1}Come importare il file.csv in Excel}{19}{section.A.1}
\contentsline {section}{\numberline {A.2}Come eliminare un valore da un data base}{20}{section.A.2}
\contentsline {section}{\numberline {A.3}Tabella pivot per la frequenza in COUNTY}{21}{section.A.3}
\contentsline {section}{\numberline {A.4}Max tra i valori POPESTIMATE*}{22}{section.A.4}
\contentsline {section}{\numberline {A.5}Grafico a linee}{23}{section.A.5}
\contentsline {section}{\numberline {A.6}Grafico a barre dei valori POPESTIMATE*}{24}{section.A.6}
\contentsline {section}{\numberline {A.7}Grafico a torta dei valori POPESTIMATE*}{25}{section.A.7}
\contentsline {chapter}{Bibliografia}{26}{figure.caption.16}
